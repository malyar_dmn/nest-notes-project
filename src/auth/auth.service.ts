import { HttpException, HttpStatus, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UsersService } from 'src/users/users.service';
import * as bcrypt from 'bcryptjs'
import { NotesService } from 'src/notes/notes.service';
import {v4} from 'uuid'

@Injectable()
export class AuthService {
    constructor(private usersService: UsersService,
                private jwtService: JwtService
                ) {}


    async login(userDto: CreateUserDto) {
        const user = await this.validateUser(userDto)
        const token = await this.generateToken(user)

        return {
            user: {id: user.id, username: user.username},
            token
        }
    }

    async registration(userDto: CreateUserDto) {
        const candidate = await this.usersService.getUserByEmail(userDto.email)
        if (candidate) {
            throw new HttpException('That user is already exists', HttpStatus.BAD_REQUEST)
        }
        // const userId = v4()

        const hashPassword = await bcrypt.hash(userDto.password, 5)
        const user = await this.usersService.createUser({...userDto, password: hashPassword})

        const token = this.generateToken(user)
        return {token}
    }

    private async generateToken(user) {
        const payload = {email: user.email, id: user.id}
        return this.jwtService.sign(payload)
    }

    private async validateUser(userDto: CreateUserDto) {
        const user = await this.usersService.getUserByEmail(userDto.email)

        if (!user) {
            throw new NotFoundException('User not found')
        }

        const passwordEquals = await bcrypt.compare(userDto.password, user.password)
        if(user && passwordEquals) {
            return user
        }
        throw new UnauthorizedException({message: 'Invalid email or password'})
    }

}
