import { Column, CreatedAt, DataType, HasMany, Model, Table } from "sequelize-typescript";
import { DataTypes } from "sequelize/dist";
import { Note } from "src/notes/notes.model";

export interface UserCreationAttrs {
    username: string
    email: string
    password: string
    id: string
}

@Table({tableName: 'users', createdAt: false, updatedAt: false})
export class User extends Model<User, UserCreationAttrs> {

    @Column({type: DataType.UUID, unique: true, primaryKey: true})
    id: string

    @Column({type: DataType.STRING, unique: true, allowNull: false})
    email: string

    @Column({type: DataType.STRING, allowNull: false})
    password: string

    @Column({type: DataType.STRING, allowNull: false})
    username: string

    @CreatedAt
    @Column({field: 'created_at', type: DataTypes.DATE, defaultValue: new Date()})
    createdAt?: Date

    @HasMany(() => Note)
    notes: Note[]
}