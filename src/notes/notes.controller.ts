import { Body, Controller, Delete, Get, Param, Post, Put, Req, UseGuards } from '@nestjs/common';
import { CreateNoteDto } from './dto/create-note.dto';
import { Note } from './notes.model';
import { NotesService } from './notes.service';

@Controller('notes')
export class NotesController {

    constructor(private notesService: NotesService) {}

    @Post()
    create(@Body() noteDto: CreateNoteDto, @Req() req: any) {
        return this.notesService.createNote(noteDto, req.user.id)
    }

    @Get()
    getAll(@Req() req: any) {
        return this.notesService.getAllNotes(req.user.id)
    }

    @Delete(':id')
    delete(@Param('id') id: string, @Req() req: any) {
        this.notesService.deleteNoteById(id, req.user.id)
    }

    @Put(':id')
    update(
        @Param('id') noteId: string,
        @Body() noteDto: Note,
        @Req() req: any
    ) {
        return this.notesService.updateNoteById(noteId, noteDto, req.user.id)
    }
}
