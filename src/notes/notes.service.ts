import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Repository } from 'sequelize-typescript';
import { User } from 'src/users/users.model';
import { v4 } from 'uuid';
import { CreateNoteDto } from './dto/create-note.dto';
import { UpdateNoteDto } from './dto/update-note.dto';
import { Note } from './notes.model';

@Injectable()
export class NotesService {

    constructor(
        @InjectModel(Note) private noteRepository: Repository<Note>,
        @InjectModel(User) private userRepository: Repository<User>,
    ) {
        // this.test().then()
    }

    async createNote(dto: CreateNoteDto, userId: string) {
        const note = await this.noteRepository.create({
            title: dto.title,
            text: dto.text,
            id: v4(),
            userId
        })
        return note
    }

    async getAllNotes(userId: string) {
        const notes = await this.noteRepository.findAll({include: {all: true}, where: {userId}})
        return notes
    }

    async deleteNoteById(id: string, userId: string) {
        const note = await this.getNoteById(id, userId)

        if (!note) {
            throw new NotFoundException('Note not found')
        }

        const res = await this.noteRepository.destroy({where: {id, userId}})

        console.log('delete id', res)
        return note.id
    }

    async getNoteById(noteId: string, userId: string) {
        const note = await this.noteRepository.findOne({include: {all: true}, where: {id: noteId, userId}})
        if (!note) {
            throw new NotFoundException('Note not found')
        }
        return note
    }

    async updateNoteById(noteId: string, noteDto: UpdateNoteDto, userId: string) {
        await this.noteRepository.update(
            {title: noteDto.title, text: noteDto.text},
            {where: {id: noteId, userId}}
        )
        
        return this.getNoteById(noteId, userId)
    }

    // async test() {
        
    //     const user = await this.userRepository.findOne({where: {email: 'dmn'}})
    //     console.log('user', user.email)

    //     if (!user) return

    //     try {
    //         const note = await this.noteRepository.create( 
    //             {
    //                 title: 'title1',
    //                 text: 'text1',
    //                 userId: user.id
    //             }
    //         )
    //     } catch (error) {
    //         console.log('error', error)
    //     }
    //   }

}
