import { Module } from '@nestjs/common';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { SequelizeModule } from '@nestjs/sequelize';
import { AuthService } from 'src/auth/auth.service';
import { User } from 'src/users/users.model';
import { UsersService } from 'src/users/users.service';
import { NotesController } from './notes.controller';
import { Note } from './notes.model';
import { NotesService } from './notes.service';

@Module({
  controllers: [NotesController],
  providers: [NotesService],
  imports: [
    SequelizeModule.forFeature([Note, User])
  ]
})
export class NotesModule {}
