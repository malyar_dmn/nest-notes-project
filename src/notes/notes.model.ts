import { BelongsTo, Column, DataType, ForeignKey, Model, Table } from "sequelize-typescript";
import { User } from "src/users/users.model";

export interface NoteCreationAttrs {
    title: string
    text: string
    userId?: string
    id: string
}


@Table({tableName: 'notes'})
export class Note extends Model<Note, NoteCreationAttrs> {
    @Column({type: DataType.UUID, unique: true, primaryKey: true})
    id: string

    @Column({type: DataType.STRING, defaultValue: ''})
    title: string

    @Column({type: DataType.STRING, defaultValue: ''})
    text: string

    @BelongsTo(() => User)
    user: User
    
    @ForeignKey(() => User)
    @Column({type: DataType.UUID})
    userId: String
}