import { Injectable, NestMiddleware, UnauthorizedException } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";

@Injectable()
export class AuthMiddleware implements NestMiddleware {
    excludedEndpoints = [
        '/auth/login',
        '/auth/registration'
    ]
    constructor(private jwtService: JwtService) {}

    private checkExeptedEndpoints(url: string): boolean {
        console.log('checkExcepted', url)
        return this.excludedEndpoints.some(item => item.indexOf(url) > -1)
    }

    use(req: any, res: any, next: () => void) {
        console.log('auth middleware')
        const isExcluded = this.checkExeptedEndpoints(req.baseUrl)
        if (isExcluded) {
            next()
            return
        }
        try { 
            const authHeader = req.headers.authorization
            const bearer = authHeader.split(' ')[0]
            const token = authHeader.split(' ')[1]


            if (bearer !== 'Bearer' || !token) {
                throw new UnauthorizedException({message: 'User is not authorized'})
            }

            console.log('token', token)
            const user = this.jwtService.verify(token)
            console.log('user', user)
            req.user = user

            next()

        } catch(e) {
            console.log('error', e)
            throw new UnauthorizedException({message: 'User is not authorized'})
        }
        
    }

    
}