import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

const cors = require('cors')

async function bootstrap() {
  const PORT = process.env.PORT || 5000
  const app = await NestFactory.create(AppModule);
  app.use(cors())
  const config = new DocumentBuilder()
        .setTitle('Nest notes')
        .setDescription('Nest documentation')
        .setVersion('1.0.0')
        .addTag('Dmn')
        .build()

  const document = SwaggerModule.createDocument(app, config)
  SwaggerModule.setup('/api/docs', app, document)

  await app.listen(PORT, () => console.log(`Server is running on port = ${PORT}`));
}
bootstrap();
